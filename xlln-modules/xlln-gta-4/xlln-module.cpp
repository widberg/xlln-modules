#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <tchar.h>
#include <vector>

TITLE_VERSION::TYPE title_version = TITLE_VERSION::tUNKNOWN;

static uint32_t GetOffsetAddress(TITLE_VERSION::TYPE version, uint32_t offset)
{
	if (offset == INVALID_OFFSET || (title_version != version && version != TITLEMODULEANY)) {
		return 0;
	}
	return (uint32_t)xlln_hmod_title + offset;
}
static uint32_t GetOffsetAddress(uint32_t offset_shipped, uint32_t offset_patch_4, uint32_t offset_patch_7, uint32_t offset_patch_8)
{
	switch (title_version) {
		case TITLE_VERSION::tTITLE_SHIPPED:
			return offset_shipped == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_shipped);
		case TITLE_VERSION::tTITLE_PATCH_4:
			return offset_patch_4 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_4);
		case TITLE_VERSION::tTITLE_PATCH_7:
			return offset_patch_7 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_7);
		case TITLE_VERSION::tTITLE_PATCH_8:
			return offset_patch_8 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_8);
	}
	return 0;
}


static uint32_t AddPatches()
{
	uint32_t patchAddress = 0;
	
	// Allow multiple instances.
	if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_7, 0x00a2cd6c)) {
		WriteBytes(patchAddress, "", sizeof(char));
	}
	
	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	uint32_t patchAddress = 0;
	
	return ERROR_SUCCESS;
}

// #41101
DWORD WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;
	
	if (XLLNSetBasePortOffsetMapping) {
		uint8_t portOffsets[]{ 0,85 };
		uint16_t portOriginals[]{ 1000,0x4321 };
		XLLNSetBasePortOffsetMapping(portOffsets, portOriginals, 2);
	}
	
	AddPatches();
	
	return result;
}

// #41102
DWORD WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;
	
	RemovePatches();
	
	return result;
}

BOOL InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256FlagFix(xlln_hmod_title);
		if (!checksumTitle) {
			return FALSE;
		}
		
		if (_strcmpi(checksumTitle, "00000000") == 0) {
			title_version = TITLE_VERSION::tTITLE_SHIPPED;
		}
		else if (_strcmpi(checksumTitle, "00000000") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_4;
		}
		else if (_strcmpi(checksumTitle, "ab21c0d90d2ca1c697785aca9dcf44635e1f8d48aa0df1d737e9190bddf0cb4c") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_7;
		}
		else if (_strcmpi(checksumTitle, "00000000") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_8;
		}
		
		free(checksumTitle);
		
		if (title_version == TITLE_VERSION::tUNKNOWN) {
			return FALSE;
		}
	}
	
	return TRUE;
}

BOOL UninitXllnModule()
{
	return TRUE;
}
