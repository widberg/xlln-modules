#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <stdint.h>
#include <tchar.h>
#include <vector>
#include <winternl.h>

TITLE_VERSION::TYPE title_version = TITLE_VERSION::tUNKNOWN;

static uint32_t GetOffsetAddress(TITLE_VERSION::TYPE version, uint32_t offset)
{
	if (offset == INVALID_OFFSET || (title_version != version && version != TITLEMODULEANY)) {
		return 0;
	}
	return (uint32_t)xlln_hmod_title + offset;
}
static uint32_t GetOffsetAddress(uint32_t offset_shipped, uint32_t offset_patch_1, uint32_t offset_patch_2, uint32_t offset_patch_3)
{
	switch (title_version) {
		case TITLE_VERSION::tTITLE_SHIPPED:
			return offset_shipped == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_shipped);
		case TITLE_VERSION::tTITLE_PATCH_1:
			return offset_patch_1 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_1);
		case TITLE_VERSION::tTITLE_PATCH_2:
			return offset_patch_2 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_2);
		case TITLE_VERSION::tTITLE_PATCH_3:
			return offset_patch_3 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_3);
	}
	return 0;
}


// #
typedef BOOL(WINAPI *tIsDebuggerPresent)();
static DWORD Import_IsDebuggerPresent = 0;
static tIsDebuggerPresent DetourIsDebuggerPresent = NULL;
static BOOL WINAPI HookIsDebuggerPresent()
{
	BOOL result = FALSE;
	
	//result = DetourIsDebuggerPresent();
	
	return result;
}

typedef NTSTATUS(WINAPI *tZwQueryInformationProcess)(HANDLE, PROCESSINFOCLASS, VOID*, ULONG, ULONG*);
static tZwQueryInformationProcess DetourZwQueryInformationProcess = NULL;
static NTSTATUS WINAPI OverrideZwQueryInformationProcess(HANDLE ProcessHandle, PROCESSINFOCLASS ProcessInformationClass, VOID *ProcessInformation, ULONG ProcessInformationLength, ULONG *ReturnLength)
{
	NTSTATUS result = 0;
	
	result = DetourZwQueryInformationProcess(ProcessHandle, ProcessInformationClass, ProcessInformation, ProcessInformationLength, ReturnLength);
	
	if (ProcessInformationClass == PROCESSINFOCLASS::ProcessDebugPort) {
		uint32_t *processDebugPort = (uint32_t*)ProcessInformation;
		*processDebugPort = 0;
	}
	
	return result;
}

#pragma pack(push, 1) // Save then set byte alignment setting.

typedef struct {
	BOOLEAN KernelDebuggerEnabled;
	BOOLEAN KernelDebuggerNotPresent;
} SYSTEM_KERNEL_DEBUGGER_INFORMATION;

#pragma pack(pop) // Return to original alignment setting.

typedef NTSTATUS(WINAPI *tZwQuerySystemInformation)(SYSTEM_INFORMATION_CLASS, VOID*, ULONG, ULONG*);
static tZwQuerySystemInformation DetourZwQuerySystemInformation = NULL;
static NTSTATUS WINAPI OverrideZwQuerySystemInformation(SYSTEM_INFORMATION_CLASS SystemInformationClass, VOID *SystemInformation, ULONG SystemInformationLength, ULONG *ReturnLength)
{
	NTSTATUS result = 0;
	
	result = DetourZwQuerySystemInformation(SystemInformationClass, SystemInformation, SystemInformationLength, ReturnLength);
	
	if (SystemInformationClass == 0x23) { // SYSTEM_INFORMATION_CLASS::SystemKernelDebuggerInformation
		SYSTEM_KERNEL_DEBUGGER_INFORMATION *DebuggerInfo = (SYSTEM_KERNEL_DEBUGGER_INFORMATION*)SystemInformation;
		DebuggerInfo->KernelDebuggerEnabled = false;
		DebuggerInfo->KernelDebuggerNotPresent = true;
	}
	
	return result;
}

// #
typedef FARPROC(WINAPI *tGetProcAddress)(HMODULE, LPCSTR);
static DWORD Import_GetProcAddress = 0;
static tGetProcAddress DetourGetProcAddress = NULL;
static FARPROC WINAPI HookGetProcAddress(HMODULE hModule, LPCSTR lpProcName)
{
	FARPROC result = 0;
	
	if (_strcmpi(lpProcName, "IsDebuggerPresent") == 0) {
		result = (FARPROC)HookIsDebuggerPresent;
	}
	else if (_strcmpi(lpProcName, "ZwQueryInformationProcess") == 0) {
		DetourZwQueryInformationProcess = (tZwQueryInformationProcess)DetourGetProcAddress(hModule, lpProcName);
		result = (FARPROC)OverrideZwQueryInformationProcess;
	}
	else if (_strcmpi(lpProcName, "ZwQuerySystemInformation") == 0) {
		DetourZwQuerySystemInformation = (tZwQuerySystemInformation)DetourGetProcAddress(hModule, lpProcName);
		result = (FARPROC)OverrideZwQuerySystemInformation;
	}
	else {
		result = DetourGetProcAddress(hModule, lpProcName);
	}
	
	return result;
}

// #5028
typedef DWORD(WINAPI *tXLiveLoadLibraryEx)(LPCWSTR lpwszModuleFileName, HINSTANCE *phModule, DWORD dwFlags);
static DWORD Import_XLiveLoadLibraryEx = 0;
static tXLiveLoadLibraryEx DetourXLiveLoadLibraryEx = NULL;
static DWORD WINAPI HookXLiveLoadLibraryEx(LPCWSTR lpwszModuleFileName, HINSTANCE *phModule, DWORD dwFlags)
{
	DWORD result;
	
	if (EndsWith(lpwszModuleFileName, L"srsw_shadowrun.dll")) {
		char *checksumModule = GetPESha256FlagFix(lpwszModuleFileName);
		if (checksumModule) {
			uint32_t moduleVersion = -1;
			if (_strcmpi(checksumModule, "0711724db7fecbcf220de258ac702028d1f5d126abc765aa0ea4184b2971823c") == 0) {
				moduleVersion = 0;
			}
			else if (_strcmpi(checksumModule, "9be93b444d8494ceca5f5b5d85b8c7332e981e633681e6c77f192044e35c96a1") == 0) {
				moduleVersion = 3;
			}
			
			free(checksumModule);
			
			// XXX Turn this off if problems occur or you do not want the module to be hooked.
			// This exists for the deobfuscated module which doesn't really have a specific checksum.
			bool hookModuleExportsAnyway = true;
			
			if (hookModuleExportsAnyway || moduleVersion != -1) {
				// Note: The offsets used below are the same for at least version 0 and 3.
				// What this does is load the dll without running dllmain (also consequently without resolving references) in order to hack some of the code in it before it is executed.
				// It is done this way so there is no need for file I/O.
				// Therefore once loaded we need to manually resolve the dll references (import dlls and each of their ordinals).
				// We then overwrite IsDebuggerPresent so that a debugger is reported to be never present while also patching the IsLicensed check to return true.
				// I thought calling LoadLibraryEx again would then execute dllmain without the OS re-resolving references but it does not appear to so we need to then also manually execute it too.
				
				result = DetourXLiveLoadLibraryEx(lpwszModuleFileName, phModule, DONT_RESOLVE_DLL_REFERENCES);
				
				HMODULE hDllSrswShadowrun = GetModuleHandle(lpwszModuleFileName);
				if (!hDllSrswShadowrun) {
					return NULL;
				}
				
				if (PEResolveImports(hDllSrswShadowrun)) {
					return NULL;
				}
				
				{
					PE_HOOK_ARG pe_hack[1];
					DWORD ordinal_addrs[2];
					WORD ordinals[] = { 569, 416 }; // IsDebuggerPresent, GetProcAddress
					char dll_name[] = "KERNEL32.dll";
					pe_hack->ordinals_len = 2;
					pe_hack->ordinal_names = NULL;
					pe_hack->ordinal_addrs = ordinal_addrs;
					pe_hack->ordinals = ordinals;
					pe_hack->pe_name = dll_name;
					pe_hack->pe_err = ERROR_FUNCTION_FAILED;
					
					if (PEImportHack(hDllSrswShadowrun, pe_hack, 1)) {
						return NULL;
					}
					
					HookImport(&Import_IsDebuggerPresent, &DetourIsDebuggerPresent, HookIsDebuggerPresent, ordinal_addrs[0]);
					HookImport(&Import_GetProcAddress, &DetourGetProcAddress, HookGetProcAddress, ordinal_addrs[1]);
				}
				
				uint32_t patchAddress = 0;
				
				if (moduleVersion == 0 || moduleVersion == 3) {
					// Patch IsLicensed function to always return true.
					patchAddress = (uint32_t)hDllSrswShadowrun + 0x0000383f;
					// MOV AL, 1
					WriteValue<uint16_t>(patchAddress, 0x01B0);
					NopFill(patchAddress + 2, 3);
					
					// Set byte value representing is licensed to 1.
					patchAddress = (uint32_t)hDllSrswShadowrun + 0x001ca588;
					*(uint8_t*)patchAddress = 1;
				}
				
				{
					IMAGE_DOS_HEADER* dos_header = (IMAGE_DOS_HEADER*)hDllSrswShadowrun;
					
					if (dos_header->e_magic != IMAGE_DOS_SIGNATURE) {
						XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_FATAL
							, "Not DOS - This file is not a DOS application."
						);
						return ERROR_BAD_EXE_FORMAT;
					}
					
					IMAGE_NT_HEADERS* nt_headers = (IMAGE_NT_HEADERS*)((DWORD)hDllSrswShadowrun + dos_header->e_lfanew);
					
					if (nt_headers->Signature != IMAGE_NT_SIGNATURE) {
						XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_FATAL
							, "Not Valid PE - This file is not a valid NT Portable Executable."
						);
						return ERROR_BAD_EXE_FORMAT;
					}
					
					DWORD entryPointAddress = (DWORD)((DWORD)hDllSrswShadowrun + (DWORD)nt_headers->OptionalHeader.AddressOfEntryPoint);
					
					BOOL (APIENTRY *entryPointFunc)(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) = (BOOL (APIENTRY *)(HMODULE, DWORD, LPVOID))entryPointAddress;
					
					entryPointFunc(hDllSrswShadowrun, DLL_PROCESS_ATTACH, 0);
				}
				
				if (moduleVersion == 0 || moduleVersion == 3) {
					// Set byte value representing is licensed to 1.
					patchAddress = (uint32_t)hDllSrswShadowrun + 0x001ca588;
					*(uint8_t*)patchAddress = 1;
				}
				
				return result;
			}
		}
	}
	
	result = DetourXLiveLoadLibraryEx(lpwszModuleFileName, phModule, dwFlags);
	
	return result;
}

typedef void(__stdcall *tLoggingHook)(int, char*, char*);
tLoggingHook pLoggingHook;
static void __stdcall LoggingHook(int a1, char *log_title, char *log_msg)
{
	uint32_t logLevel = XLLN_LOG_CONTEXT_OTHER | XLLN_LOG_LEVEL_DEBUG;
	
	XLLN_DEBUG_LOG(logLevel, "0x%x %s %s", a1, log_title, log_msg);
	
	pLoggingHook(a1, log_title, log_msg);
}

static uint32_t AddPatches()
{
	uint32_t patchAddress = 0;
	DWORD dwBack;
	
	if ((patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0)) || (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0))) {
		PE_HOOK_ARG pe_hack[1];
		DWORD ordinal_addrs[1];
		// #5028 XLiveLoadLibraryEx
		WORD ordinals[1] = { 5028 };
		char dll_name[] = "xlive.dll";
		pe_hack->ordinals_len = 1;
		pe_hack->ordinal_names = NULL;
		pe_hack->ordinal_addrs = ordinal_addrs;
		pe_hack->ordinals = ordinals;
		pe_hack->pe_name = dll_name;
		pe_hack->pe_err = ERROR_FUNCTION_FAILED;
		
		if (PEImportHack(xlln_hmod_title, pe_hack, 1)) {
			return FALSE;
		}
		
		HookImport(&Import_XLiveLoadLibraryEx, &DetourXLiveLoadLibraryEx, HookXLiveLoadLibraryEx, ordinal_addrs[0]);
	}
	
	// Improve existing windowed mode.
	if ((patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x0000603f + 7)) || (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x00005f7b + 7))) {
		WriteValue<uint32_t>(patchAddress, WS_DLGFRAME | WS_BORDER | WS_CAPTION | WS_TABSTOP | WS_GROUP | WS_SIZEBOX | WS_SYSMENU);
	}
	//if ((patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x002ff01a + 1)) || (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x0030d00a + 1))) {
	//	WriteValue<uint32_t>(patchAddress, WS_DLGFRAME | WS_BORDER | WS_CAPTION | WS_TABSTOP | WS_GROUP | WS_SIZEBOX | WS_SYSMENU);
	//}
	if ((patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x002ff048 + 1)) || (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x0030d038 + 1))) {
		WriteValue<uint32_t>(patchAddress, WS_DLGFRAME | WS_BORDER | WS_CAPTION | WS_TABSTOP | WS_GROUP | WS_SIZEBOX | WS_SYSMENU);
	}
	
	// Hook native logging function.
	if ((patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x0026e840)) || (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x0027b590))) {
		pLoggingHook = (tLoggingHook)DetourFunc((uint8_t*)patchAddress, (uint8_t*)LoggingHook, 7);
		VirtualProtect(pLoggingHook, 4, PAGE_EXECUTE_READWRITE, &dwBack);
	}
	
	// Change the function that verifies the given data with the cryptographic signature to always return true.
	if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x00087730)) {
		//xor eax, eax
		//mov al, 1
		//retn
		uint8_t assemblyInstructions[] = { 0x33, 0xC0, 0xB0, 0x01, 0xC3 };
		WriteBytes(patchAddress, assemblyInstructions, sizeof(assemblyInstructions));
		NopFill(patchAddress + sizeof(assemblyInstructions), 2);
	}
	
	// NOP out conditional jump that would signify data does not match cryptographic signature.
	//if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x0008778b)) {
	//	NopFill(patchAddress, 2);
	//}
	
	// Patch out checksum verification.
	//if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x00098544)) {
	//	WriteValue<uint8_t>(patchAddress, 0xEB);
	//}
	
	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	uint32_t patchAddress = 0;
	
	// Undo hooks.
	HookImport(&Import_XLiveLoadLibraryEx, &DetourXLiveLoadLibraryEx, HookXLiveLoadLibraryEx, NULL);
	// We should probably hook release library for unhooking this IsDebuggerPresent one.
	//HookImport(&Import_IsDebuggerPresent, &DetourIsDebuggerPresent, HookIsDebuggerPresent, NULL);
	
	if ((patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x0026e840)) || (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x0027b590))) {
		RetourFunc((uint8_t*)patchAddress, (uint8_t*)pLoggingHook, 7);
		free(pLoggingHook);
	}
	
	return ERROR_SUCCESS;
}

// #41101
DWORD WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;
	
	// if (XLLNSetBasePortOffsetMapping) {
	// 	uint8_t portOffsets[]{ 0,10 };
	// 	uint16_t portOriginals[]{ 1000,1010 };
	// 	XLLNSetBasePortOffsetMapping(portOffsets, portOriginals, 2);
	// }
	
	AddPatches();
	
	return result;
}

// #41102
DWORD WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;
	
	RemovePatches();
	
	return result;
}

BOOL InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256FlagFix(xlln_hmod_title);
		if (!checksumTitle) {
			return FALSE;
		}
		
		if (_strcmpi(checksumTitle, "360a7a55194b52343113dd9a42a89d16ec82c61e0ceb390f569f0cde090e5c52") == 0) {
			title_version = TITLE_VERSION::tTITLE_SHIPPED;
		}
		else if (_strcmpi(checksumTitle, "00000000") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_1;
		}
		else if (_strcmpi(checksumTitle, "00000000") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_2;
		}
		else if (_strcmpi(checksumTitle, "c03c4e4e5b04ed972b013b5a124c8e37b339b5c58a1ae82ff1f2c0df95e6d567") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_3;
		}
		
		free(checksumTitle);
		
		if (title_version == TITLE_VERSION::tUNKNOWN) {
			return FALSE;
		}
	}
	
	return TRUE;
}

BOOL UninitXllnModule()
{
	return TRUE;
}
