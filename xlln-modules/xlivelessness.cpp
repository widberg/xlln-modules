#include <Winsock2.h>
#include "dllmain.hpp"
#include "xlivelessness.hpp"

// #41140
tXLLNLogin XLLNLogin;

// #41141
tXLLNLogout XLLNLogout;

// #41142
tXLLNModifyProperty XLLNModifyProperty;

// #41143
tXLLNDebugLog XLLNDebugLog;

// #41144
tXLLNDebugLogF XLLNDebugLogF;

// #41145
tXLLNGetXLLNStoragePath XLLNGetXLLNStoragePath;

// #41146
tXLLNSetBasePortOffsetMapping XLLNSetBasePortOffsetMapping;

// #41147
tXLLNGetDebugLogLevel XLLNGetDebugLogLevel;

static uint32_t __stdcall iXLLNGetXLLNStoragePath(uint32_t module_handle, uint32_t *result_local_instance_id, wchar_t *result_storage_path_buffer, size_t *result_storage_path_buffer_size)
{
	return ERROR_INVALID_FUNCTION;
}

static uint32_t __stdcall iXLLNModifyProperty(XLLNModifyPropertyTypes::TYPE propertyId, uint32_t *newValue, uint32_t *oldValue)
{
	return ERROR_INVALID_FUNCTION;
}

BOOL InitXLiveLessNess()
{
	// Get XLLN exports.
	
	XLLNLogin = (tXLLNLogin)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41140);
	if (!XLLNLogin) {
	}
	XLLNLogout = (tXLLNLogout)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41141);
	if (!XLLNLogout) {
	}
	XLLNModifyProperty = (tXLLNModifyProperty)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41142);
	if (!XLLNModifyProperty) {
		XLLNModifyProperty = iXLLNModifyProperty;
	}
	XLLNDebugLog = (tXLLNDebugLog)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41143);
	if (!XLLNDebugLog) {
	}
	XLLNDebugLogF = (tXLLNDebugLogF)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41144);
	if (!XLLNDebugLogF) {
	}
	XLLNGetXLLNStoragePath = (tXLLNGetXLLNStoragePath)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41145);
	if (!XLLNGetXLLNStoragePath) {
		XLLNGetXLLNStoragePath = iXLLNGetXLLNStoragePath;
	}
	XLLNSetBasePortOffsetMapping = (tXLLNSetBasePortOffsetMapping)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41146);
	if (!XLLNSetBasePortOffsetMapping) {
	}
	XLLNGetDebugLogLevel = (tXLLNGetDebugLogLevel)GetProcAddress(xlln_hmod_xlivelessness, (PCSTR)41147);
	if (!XLLNGetDebugLogLevel) {
	}

	return TRUE;
}

BOOL UninitXLiveLessNess()
{

	return TRUE;
}
