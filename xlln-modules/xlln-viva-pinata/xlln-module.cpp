#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <tchar.h>
#include <vector>

TITLE_VERSION::TYPE title_version = TITLE_VERSION::tUNKNOWN;

static uint32_t GetOffsetAddress(TITLE_VERSION::TYPE version, uint32_t offset)
{
	if (offset == INVALID_OFFSET || (title_version != version && version != TITLEMODULEANY)) {
		return 0;
	}
	return (uint32_t)xlln_hmod_title + offset;
}

static uint32_t AddPatches()
{
	uint32_t patchAddress = 0;
	
	// Allow multiple instances by always jumping instead of conditional jump if it does not find existing window with same name.
	// Does not work, it tries to boot but freezes before intro can be played.
	//if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x004c1c1b)) {
	//	WriteValue<uint8_t>(patchAddress, 0xEB);
	//}
	//if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x004c16c7)) {
	//	WriteValue<uint8_t>(patchAddress, 0xEB);
	//}
	
	// NOP function calls out that checks if the launcher is running.
	if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x004c1c5d)) {
		NopFill(patchAddress, 0x1A);
	}
	
	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	uint32_t patchAddress = 0;
	
	return ERROR_SUCCESS;
}

// #41101
DWORD WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;
	
	if (XLLNSetBasePortOffsetMapping) {
		uint8_t portOffsets[]{ 0,1 };
		uint16_t portOriginals[]{ 1000,1001 };
		XLLNSetBasePortOffsetMapping(portOffsets, portOriginals, 2);
	}
	
	AddPatches();
	
	return result;
}

// #41102
DWORD WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;
	
	RemovePatches();
	
	return result;
}

BOOL InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256FlagFix(xlln_hmod_title);
		if (!checksumTitle) {
			return FALSE;
		}
		
		if (_strcmpi(checksumTitle, "0a619346e491544b60b92cbc953ac34ebe7c103ddcf35eb16b63c6a72a2e9513") == 0) {
			title_version = TITLE_VERSION::tTITLE_SHIPPED;
		}
		
		free(checksumTitle);
		
		if (title_version == TITLE_VERSION::tUNKNOWN) {
			return FALSE;
		}
	}
	
	return TRUE;
}

BOOL UninitXllnModule()
{
	return TRUE;
}
