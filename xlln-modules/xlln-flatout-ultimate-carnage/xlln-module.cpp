#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <tchar.h>
#include <vector>

static uint32_t AddPatches()
{
	// Jump over -fromlauncher argument check.
	PatchWithJump(0x0055b48a, 0x0055b4bc);
	// Jump over the function that checks if the launcher is running and has verified.
	PatchWithJump(0x0055b4f5, 0x0055b55d);

	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	return ERROR_SUCCESS;
}

// #41101
DWORD WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;

	AddPatches();

	return result;
}

// #41102
DWORD WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;

	RemovePatches();

	return result;
}

BOOL InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256FlagFix(xlln_hmod_title);
		if (!checksumTitle) {
			return FALSE;
		}

		bool titleImageSha256Matches = _strcmpi(checksumTitle, "faf3dfb0f6d408963a30b8059fede248bd63900f4e2aeee7b54cec6a9d9643e9") == 0;

		free(checksumTitle);

		if (!titleImageSha256Matches) {
			return FALSE;
		}
	}

	return TRUE;
}

BOOL UninitXllnModule()
{
	return TRUE;
}
