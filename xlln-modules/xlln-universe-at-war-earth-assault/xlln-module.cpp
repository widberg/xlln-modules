#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <tchar.h>
#include <vector>

TITLE_VERSION::TYPE title_version = TITLE_VERSION::tUNKNOWN;

static uint32_t GetOffsetAddress(TITLE_VERSION::TYPE version, uint32_t offset)
{
	if (offset == INVALID_OFFSET || (title_version != version && version != TITLEMODULEANY)) {
		return 0;
	}
	return (uint32_t)xlln_hmod_title + offset;
}
static uint32_t GetOffsetAddress(uint32_t offset_shipped, uint32_t offset_patch_1, uint32_t offset_patch_2, uint32_t offset_patch_3)
{
	switch (title_version) {
		case TITLE_VERSION::tTITLE_SHIPPED:
			return offset_shipped == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_shipped);
		case TITLE_VERSION::tTITLE_PATCH_1:
			return offset_patch_1 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_1);
		case TITLE_VERSION::tTITLE_PATCH_2:
			return offset_patch_2 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_2);
		case TITLE_VERSION::tTITLE_PATCH_3:
			return offset_patch_3 == INVALID_OFFSET ? 0 : ((uint32_t)xlln_hmod_title + offset_patch_3);
	}
	return 0;
}

static void CreateReverseLauncherPipe()
{
	FILE* fileHandle = 0;
	errno_t errOpen = fopen_s(&fileHandle, "./LaunchUAW.exe", "rb");
	if (!fileHandle || errOpen) {
		return;
	}
	fseek(fileHandle, (long)0, SEEK_END);
	size_t fileSize = ftell(fileHandle);
	fseek(fileHandle, (long)0, SEEK_SET);
	fileSize -= ftell(fileHandle);
	uint8_t *buffer = new uint8_t[fileSize];
	size_t readC = fread(buffer, sizeof(uint8_t), fileSize / sizeof(uint8_t), fileHandle);
	fclose(fileHandle);
	fileHandle = 0;
	
	if (readC != fileSize) {
		delete[] buffer;
		return;
	}
	
	char *checksum = GetSha256Sum(buffer, fileSize);
	
	if (_strcmpi(checksum, "b1de33e1c169ed2263a4562e828b4d34d6689e1b271e62fcd6e71526d406b833") == 0) {
		// Fix the checksum of the new binary.
		*(uint16_t*)&buffer[0x138] = 0x557D;
		// Set wait parameter to forever (-1).
		*(uint32_t*)&buffer[0x732] = 0xFFFFFFFF;
		*(uint32_t*)&buffer[0x7d4] = 0xFFFFFFFF;
		// Do not launch the game.
		uint8_t patch[] = { 0x68, 0x58, 0x90, 0x44, 0x00, 0xE8, 0x46, 0xE1, 0xFF, 0xFF, 0x83, 0xC4, 0x04, 0x6A, 0x00, 0xFF, 0x15, 0xA0, 0x41, 0x41, 0x00, 0x50, 0xFF, 0x15, 0xC8, 0x40, 0x41, 0x00, 0x90, 0x90, 0x90, 0x90, 0x90 };
		memcpy((uint32_t*)&buffer[0x2820], patch, sizeof(patch));
	}
	else {
		delete[] checksum;
		delete[] buffer;
		return;
	}
	
	delete[] checksum;
	
	errOpen = fopen_s(&fileHandle, "./LaunchUAW-Pipe.exe", "wb+");
	if (!fileHandle || errOpen) {
		return;
	}
	
	fwrite(buffer, sizeof(uint8_t), fileSize, fileHandle);
	
	delete[] buffer;
	
	fclose(fileHandle);
}

static bool ValidNormalChar(char letter)
{
	if (
		(letter >= 'a' && letter <= 'z')
		|| (letter >= 'A' && letter <= 'Z')
		|| (letter >= '0' && letter <= '9')
	) {
		return true;
	}
	switch (letter) {
		case ' ':
		case '\t':
		case '~':
		case '`':
		case '!':
		case '@':
		case '#':
		case '$':
		case '%':
		case '^':
		case '&':
		case '*':
		case '(':
		case ')':
		case '-':
		case '_':
		case '+':
		case '=':
		case '{':
		case '[':
		case '}':
		case ']':
		case ';':
		case ':':
		case '\'':
		case '\"':
		case '\\':
		case '|':
		case '<':
		case ',':
		case '>':
		case '.':
		case '/':
		case '?':
			return true;
	}
	return false;
}

static uint32_t func_000437c7 = 0;
static uint32_t func_chunk_rtn_to_0004386d = 0;

__declspec(naked) void nak_func_chunk_00043865()
{
	__asm {
		push esi
		mov esi, ecx
		call func_000437c7
		jmp func_chunk_rtn_to_0004386d
	}
}

static uint32_t __cdecl UaWLogger(const char *const format, ...)
{
	uint32_t logLevel = XLLN_LOG_CONTEXT_OTHER | XLLN_LOG_LEVEL_DEBUG;
	
	if (!XLLNGetDebugLogLevel || !XLLNDebugLog) {
		return ERROR_SUCCESS;
	}
	
	uint32_t xllnDebugLogLevel = 0;
	if (XLLNGetDebugLogLevel(&xllnDebugLogLevel)) {
		return ERROR_SUCCESS;
	}
	
	if (!(logLevel & xllnDebugLogLevel & XLLN_LOG_CONTEXT_MASK) || !(logLevel & xllnDebugLogLevel & XLLN_LOG_LEVEL_MASK)) {
		return ERROR_SUCCESS;
	}
	
	// An attempt to filter out all the bad logging calls with no real formatter string (likely because they were compiled without them).
	if (
		(uint32_t)format < (uint32_t)xlln_hmod_title
		|| (uint32_t)format > (uint32_t)xlln_hmod_title + 0x00a444d0
		|| !ValidNormalChar(format[0])
		|| !ValidNormalChar(format[1])
		|| !ValidNormalChar(format[2])
	) {
		return ERROR_SUCCESS;
	}
	
	auto temp = std::vector<char>{};
	auto length = std::size_t{ 63 };
	va_list args;
	while (temp.size() <= length) {
		temp.resize(length + 1);
		va_start(args, format);
		const auto status = std::vsnprintf(temp.data(), temp.size(), format, args);
		va_end(args);
		if (status < 0) {
			// string formatting error.
			return ERROR_INVALID_PARAMETER;
		}
		length = static_cast<std::size_t>(status);
	}
	
	if (length <= 0) {
		return ERROR_SUCCESS;
	}
	// Remove the new line formatter on the end.
	if (temp.data()[length - 1] == '\n') {
		temp.data()[--length] = 0;
	}
	if (length <= 0) {
		return ERROR_SUCCESS;
	}
	
	XLLNDebugLog(logLevel, std::string{ temp.data(), length }.c_str());
	
	return ERROR_SUCCESS;
}

static uint32_t AddPatches()
{
	uint32_t patchAddress = 0;
	
	// Allow multiple instances.
	if (patchAddress = GetOffsetAddress(0x0000d5e8, 0x0000e49c, 0x0000e64e, 0x0047f26d)) {
		// Change to JMP.
		WriteValue<uint8_t>(patchAddress, 0xEB);
	}
	
	int nArgs;
	// GetCommandLineW() does not need de-allocating but ToArgv does.
	LPWSTR* lpwszArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	if (lpwszArglist == NULL) {
		uint32_t errorCmdLineToArgv = GetLastError();
		return ERROR_INVALID_FLAGS;
	}
	
	for (int i = 1; i < nArgs; i++) {
		// Set to windowed mode.
		if (wcscmp(lpwszArglist[i], L"-windowed") == 0) {
			if (patchAddress = GetOffsetAddress(0x006cc384, 0x006d13e8, 0x006db418, 0x005e1358)) {
				WriteValue(patchAddress, WS_DLGFRAME | WS_BORDER | WS_CAPTION | WS_TABSTOP | WS_GROUP | WS_SIZEBOX | WS_SYSMENU);
			}
			// Set windowed mode uint8_t flag to 1.
			if (patchAddress = GetOffsetAddress(0x0000d6a2 + 6, 0x0000e552 + 6, 0x0000e704 + 6, INVALID_OFFSET)) {
				WriteValue<uint8_t>(patchAddress, 0x01);
			}
			if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x0047f33a)) {
				NopFill(patchAddress, 6);
			}
			if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x007aea1d)) {
				WriteValue<uint8_t>(patchAddress, 0x01);
			}
		}
		
		// Launches a modified version of the launcher that does not launch the game and will wait forever without closing in-case the game is paused for debugging.
		// The purpose of this is the game does not work when running without the launcher. Additionally there is some complicated obfuscated data transferred back and fourth and without this pipe the game cannot decrypt or decode required values and crashes/becomes buggy.
		else if (wcscmp(lpwszArglist[i], L"-reverselauncher") == 0) {
			CreateReverseLauncherPipe();
			
			LPTSTR szCmdline = _tcsdup(TEXT("LaunchUAW-Pipe.exe"));
			STARTUPINFO si = { sizeof(si) };
			PROCESS_INFORMATION pi;
			
			BOOL startSuccess = CreateProcessW(0, szCmdline, NULL, NULL, FALSE, CREATE_NEW_PROCESS_GROUP, NULL, NULL, &si, &pi);
			if (startSuccess) {
				wchar_t *pipeName = FormMallocString(L"\\\\.\\pipe\\%08x", pi.dwProcessId);
				
				if (patchAddress = GetOffsetAddress(0x0050b5fb + 1, 0x0050d55b + 1, 0x005168cb + 1, 0x004d3bde + 1)) {
					WritePointer(patchAddress, (void*)pipeName);
				}
			}
		}
	}
	
	LocalFree(lpwszArglist);
	lpwszArglist = 0;
	
	uint16_t xliveBasePort = 0;
	if (XLLNModifyProperty && XLLNModifyProperty(XLLNModifyPropertyTypes::tBASE_PORT, 0, (uint32_t*)&xliveBasePort) == ERROR_SUCCESS && IsUsingBasePort(xliveBasePort)) {
		// NOP out the conditional JMP (so we do not bind to the Packet Handler port provided by the func args).
		if (patchAddress = GetOffsetAddress(0x0005db1a, 0x0005fcfa, 0x000605fa, INVALID_OFFSET)) {
			NopFill(patchAddress, 2);
		}
		// Change JMP to unconditional (so we do not bind to the Packet Handler port provided by the func args).
		if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x004b25aa)) {
			WriteValue<uint8_t>(patchAddress, 0xEB);
		}
		// Set the (new) fall-back Packet Handler port.
		if (patchAddress = GetOffsetAddress(0x0005db1c + 1, 0x0005fcfc + 1, 0x000605fc + 1, 0x004b25a5 + 1)) {
			WriteValue<uint32_t>(patchAddress, 2001);
		}
		
		// NOP out the conditional JMP (so we do not bind to the Voice Chat Handler port provided by the func args).
		if (patchAddress = GetOffsetAddress(0x000645b5, 0x000669f5, 0x00067415, INVALID_OFFSET)) {
			NopFill(patchAddress, 2);
		}
		// Change JMP to unconditional (so we do not bind to the Voice Chat Handler port provided by the func args).
		if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x004b6c83)) {
			WriteValue<uint8_t>(patchAddress, 0xEB);
		}
		// Set the (new) fall-back Voice Chat Handler port.
		if (patchAddress = GetOffsetAddress(0x000645b7 + 1, 0x000669f7 + 1, 0x00067417 + 1, 0x004b6c7e + 1)) {
			WriteValue<uint32_t>(patchAddress, 2002);
		}
	}
	
	// If debug mode is active then enable this logger.
	uint32_t xllnDebugLogLevel = 0;
	if (XLLNGetDebugLogLevel && XLLNGetDebugLogLevel(&xllnDebugLogLevel) == ERROR_SUCCESS) {
		if (patchAddress = GetOffsetAddress(0x0050cd90, 0x000ace10, 0x0008e190, 0x00043864)) {
			PatchWithJump(patchAddress, UaWLogger);
		}
		
		// Patch 3 doesn't have padding after the stubbed function so we need to make room by moving what was already there.
		if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x00508dc7)) {
			func_000437c7 = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x000437c7);
			func_chunk_rtn_to_0004386d = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x0004386d);
			
			PatchWithJump(patchAddress, nak_func_chunk_00043865);
		}
	}
	
	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	uint32_t patchAddress = 0;
	
	// Undo the Logging Hook (even if it was not active it would not change the data that should already be there).
	{
		// Change back to RETN.
		if (patchAddress = GetOffsetAddress(0x0050cd90, 0x000ace10, 0x0008e190, 0x00043864)) {
			WriteValue<uint8_t>(patchAddress, 0xC3);
		}
		
		// Change back to padding.
		if (patchAddress = GetOffsetAddress(0x0050cd90 + 1, 0x000ace10 + 1, 0x0008e190 + 1, INVALID_OFFSET)) {
			WriteValue<uint32_t>(patchAddress, 0xCCCCCCCC);
		}
		// Patch 3 doesn't have padding after the stubbed function so we need to revert what was originally there.
		if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x00508dc7)) {
			uint32_t func_chunk_00043865 = GetOffsetAddress(TITLE_VERSION::tTITLE_PATCH_3, 0x00043865);
			
			// Push ESI
			WriteValue<uint8_t>(func_chunk_00043865, 0x56);
			// MOV ESI, ECX
			WriteValue<uint16_t>(func_chunk_00043865 + 1, 0xF18B);
			
			PatchWithCall(func_chunk_00043865 + 1 + 2, func_000437c7);
			
			PatchWithJump(patchAddress, func_chunk_00043865);
		}
	}
	
	return ERROR_SUCCESS;
}

// #41101
DWORD WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;
	
	if (XLLNSetBasePortOffsetMapping) {
		uint8_t portOffsets[]{ 1,2 };
		uint16_t portOriginals[]{ 2200,1278 };
		XLLNSetBasePortOffsetMapping(portOffsets, portOriginals, 2);
	}
	
	AddPatches();
	
	return result;
}

// #41102
DWORD WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;
	
	RemovePatches();
	
	return result;
}

BOOL InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256FlagFix(xlln_hmod_title);
		if (!checksumTitle) {
			return FALSE;
		}
		
		if (_strcmpi(checksumTitle, "5a1e7e33387892c1287facdfd21f75994c7c306388afd22d7d5ec5f1a365cc6a") == 0) {
			title_version = TITLE_VERSION::tTITLE_SHIPPED;
		}
		else if (_strcmpi(checksumTitle, "9dec30382798c38fbe190da05cff0cbbb923b4a5ada9b7dbfce3e16816dc987a") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_1;
		}
		else if (_strcmpi(checksumTitle, "21cd31086e501052505348da238d49fd30f9b1e723d6661d53e159b0467d6d84") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_2;
		}
		else if (_strcmpi(checksumTitle, "ef707daaaba7d1779f1312fdcc58c2e2336897ebf7e9e3cfca6cabc07943958c") == 0) {
			title_version = TITLE_VERSION::tTITLE_PATCH_3;
		}
		
		free(checksumTitle);
		
		if (title_version == TITLE_VERSION::tUNKNOWN) {
			return FALSE;
		}
	}
	
	return TRUE;
}

BOOL UninitXllnModule()
{
	return TRUE;
}
