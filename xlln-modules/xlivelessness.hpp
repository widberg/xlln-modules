#pragma once
#include <stdint.h>

#define XLLN_LOG_LEVEL_MASK		0b00111111
// Function call tracing.
#define XLLN_LOG_LEVEL_TRACE	0b00000001
// Function, variable and operation logging.
#define XLLN_LOG_LEVEL_DEBUG	0b00000010
// Generally useful information to log (service start/stop, configuration assumptions, etc).
#define XLLN_LOG_LEVEL_INFO		0b00000100
// Anything that can potentially cause application oddities, but is being handled adequately.
#define XLLN_LOG_LEVEL_WARN		0b00001000
// Any error which is fatal to the operation, but not the service or application (can't open a required file, missing data, etc.).
#define XLLN_LOG_LEVEL_ERROR	0b00010000
// Errors that will terminate the application.
#define XLLN_LOG_LEVEL_FATAL	0b00100000

#define XLLN_LOG_CONTEXT_MASK			(0b10000111 << 8)
// Logs related to Xlive(GFWL) functionality.
#define XLLN_LOG_CONTEXT_XLIVE			(0b00000001 << 8)
// Logs related to XLiveLessNess functionality.
#define XLLN_LOG_CONTEXT_XLIVELESSNESS	(0b00000010 << 8)
// Logs related to XLLN-Module functionality.
#define XLLN_LOG_CONTEXT_XLLN_MODULE	(0b00000100 << 8)
// Logs related to functionality from other areas of the application.
#define XLLN_LOG_CONTEXT_OTHER			(0b10000000 << 8)

#define IsUsingBasePort(base_port) (base_port != 0 && base_port != 0xFFFF)

namespace XLLNModifyPropertyTypes {
	const char* const TypeNames[] {
		"UNKNOWN",
		"FPS_LIMIT",
		"LiveOverLan_BROADCAST_HANDLER",
		"RECVFROM_CUSTOM_HANDLER_REGISTER",
		"RECVFROM_CUSTOM_HANDLER_UNREGISTER",
		"BASE_PORT",
		"GUIDE_UI_HANDLER",
		"XHV_ENGINE_ENABLED",
	};
	typedef enum : BYTE {
		tUNKNOWN = 0,
		tFPS_LIMIT,
		tLiveOverLan_BROADCAST_HANDLER,
		tRECVFROM_CUSTOM_HANDLER_REGISTER,
		tRECVFROM_CUSTOM_HANDLER_UNREGISTER,
		tBASE_PORT,
		tGUIDE_UI_HANDLER,
		tXHV_ENGINE_ENABLED,
	} TYPE;

#pragma pack(push, 1) // Save then set byte alignment setting.

	typedef struct {
		char *Identifier;
		uint32_t *FuncPtr;
	} RECVFROM_CUSTOM_HANDLER_REGISTER;

#pragma pack(pop) // Return to original alignment setting.

}

// #41140
typedef uint32_t(WINAPI *tXLLNLogin)(uint32_t dwUserIndex, BOOL bLiveEnabled, uint32_t dwUserId, const CHAR *szUsername);
// #41141
typedef uint32_t(WINAPI *tXLLNLogout)(uint32_t dwUserIndex);
// #41142
typedef uint32_t(WINAPI *tXLLNModifyProperty)(XLLNModifyPropertyTypes::TYPE propertyId, uint32_t *newValue, uint32_t *oldValue);
// #41143
typedef uint32_t(WINAPI *tXLLNDebugLog)(uint32_t logLevel, const char *message);
// #41144
typedef uint32_t(WINAPI *tXLLNDebugLogF)(uint32_t logLevel, const char *const format, ...);
// #41145
typedef uint32_t(__stdcall *tXLLNGetXLLNStoragePath)(uint32_t module_handle, uint32_t *result_local_instance_id, wchar_t *result_storage_path_buffer, size_t *result_storage_path_buffer_size);
// #41146
typedef uint32_t(__stdcall *tXLLNSetBasePortOffsetMapping)(uint8_t *port_offsets, uint16_t *port_originals, uint8_t port_mappings_size);
// #41147
typedef uint32_t (__stdcall *tXLLNGetDebugLogLevel)(uint32_t *log_level);

extern tXLLNLogin XLLNLogin;
extern tXLLNLogout XLLNLogout;
extern tXLLNModifyProperty XLLNModifyProperty;
extern tXLLNDebugLog XLLNDebugLog;
extern tXLLNDebugLogF XLLNDebugLogF;
extern tXLLNGetXLLNStoragePath XLLNGetXLLNStoragePath;
extern tXLLNSetBasePortOffsetMapping XLLNSetBasePortOffsetMapping;
extern tXLLNGetDebugLogLevel XLLNGetDebugLogLevel;

BOOL InitXLiveLessNess();
BOOL UninitXLiveLessNess();

#ifdef _DEBUG
// Move this out to enable building with debug logs in release mode.
#define XLLN_DEBUG
#endif

#ifdef XLLN_DEBUG
#define XLLN_DEBUG_LOG(logLevel, format, ...) if (XLLNDebugLogF) { XLLNDebugLogF(logLevel, format, __VA_ARGS__); }
#else
#define XLLN_DEBUG_LOG(logLevel, format, ...)
#endif
