#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <tchar.h>
#include <vector>

TITLE_VERSION::TYPE title_version = TITLE_VERSION::tUNKNOWN;

static uint32_t GetOffsetAddress(TITLE_VERSION::TYPE version, uint32_t offset)
{
	if (offset == INVALID_OFFSET || (title_version != version && version != TITLEMODULEANY)) {
		return 0;
	}
	return (uint32_t)xlln_hmod_title + offset;
}

static uint32_t AddPatches()
{
	uint32_t patchAddress = 0;
	
	// Change the function that checks if the launcher is running and has verified to instead return immediately without error.
	if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x0064c250)) {
		//xor eax, eax
		//retn
		uint8_t assemblyInstructions[] = { 0x33, 0xC0, 0xC3 };
		WriteBytes(patchAddress, assemblyInstructions, sizeof(assemblyInstructions));
	}
	
	// Allow multiple instances of the game by removing the name from what would be a globally unique named mutex.
	if (patchAddress = GetOffsetAddress(TITLE_VERSION::tTITLE_SHIPPED, 0x004f82f8)) {
		//push 0
		//nop
		//nop
		//nop
		uint8_t assemblyInstructions[] = { 0x6A, 0x00, 0x90, 0x90, 0x90 };
		WriteBytes(patchAddress, assemblyInstructions, sizeof(assemblyInstructions));
	}
	
	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	uint32_t patchAddress = 0;
	
	return ERROR_SUCCESS;
}

// #41101
DWORD WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;
	
	AddPatches();
	
	return result;
}

// #41102
DWORD WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;
	
	RemovePatches();
	
	return result;
}

BOOL InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256FlagFix(xlln_hmod_title);
		if (!checksumTitle) {
			return FALSE;
		}
		
		if (_strcmpi(checksumTitle, "a70205447009e100601348bf0962180b4de214e1192af9c918e7af5e5763f7fc") == 0) {
			//TODO: I do not know exactly which version this is. It is likely Patch 2. Until more information appears I will leave it as this.
			title_version = TITLE_VERSION::tTITLE_SHIPPED;
		}
		
		free(checksumTitle);
		
		if (title_version == TITLE_VERSION::tUNKNOWN) {
			return FALSE;
		}
	}
	
	return TRUE;
}

BOOL UninitXllnModule()
{
	return TRUE;
}
