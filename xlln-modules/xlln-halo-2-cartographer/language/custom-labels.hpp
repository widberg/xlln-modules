#pragma once

BOOL InitCustomLabels();
BOOL UninitCustomLabels();

extern const int CMLabelMenuId_Error;
extern const int CMLabelMenuId_Language;
extern const int CMLabelMenuId_Guide;
extern const int CMLabelMenuId_EscSettings;
extern const int CMLabelMenuId_AdvSettings;
extern const int CMLabelMenuId_Credits;
extern const int CMLabelMenuId_EditHudGui;
extern const int CMLabelMenuId_ToggleSkulls;
extern const int CMLabelMenuId_AccountList;
extern const int CMLabelMenuId_AccountAdd;
extern const int CMLabelMenuId_VKeyTest;
extern const int CMLabelMenuId_AccountCreate;
extern const int CMLabelMenuId_AdvOtherSettings;
extern const int CMLabelMenuId_EditFPS;
extern const int CMLabelMenuId_EditFOV;
extern const int CMLabelMenuId_EditFOVVehicle;
extern const int CMLabelMenuId_EditCrosshairOffset;
extern const int CMLabelMenuId_EditCrosshairSize;
extern const int CMLabelMenuId_Update;
extern const int CMLabelMenuId_Update_Note;
extern const int CMLabelMenuId_Login_Warn;
extern const int CMLabelMenuId_EditStaticLoD;
extern const int CMLabelMenuId_AdvLobbySettings;
extern const int CMLabelMenuId_Invalid_Login_Token;
extern const int CMLabelMenuId_AccountManagement;
extern const int CMLabelMenuId_AdvControlsSettings;
extern const int CMLabelMenuId_EditSensitivityNativeController;
extern const int CMLabelMenuId_EditSensitivityNativeMouse;
extern const int CMLabelMenuId_EditSensitivityRawMouse;
extern const int CMLabelMenuId_EditSensitivityXYAxisRatioController;
extern const int CMLabelMenuId_EditSensitivityXYAxisRatioMouse;
extern const int CMLabelMenuId_EditSensitivityVehicleRatioController;
extern const int CMLabelMenuId_EditSensitivityVehicleRatioMouse;
extern const int CMLabelMenuId_ChangeBsp;
extern const int CMLabelMenuId_ChangeDifficulty;
extern const int CMLabelMenuId_CoopMode;
extern const int CMLabelMenuId_EditFpsLimit;

extern int CM_Number_Of_Buttons_Credits;
